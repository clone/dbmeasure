CFLAGS=-Wextra -Wall -O0 -g -pipe `pkg-config --cflags alsa`
LIBS=`pkg-config --libs alsa` -lm

all: dbmeasure dbverify

dbmeasure: dbmeasure.o
	$(CC) -o dbmeasure $^ $(CFLAGS) $(LIBS)

dbverify: dbverify.o
	$(CC) -o dbverify $^ $(CFLAGS) $(LIBS)

clean:
	rm -f *.o dbmeasure dbverify
